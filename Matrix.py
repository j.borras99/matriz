import random

class Matrix:
    ##Create a new Matrix
    def __init__(self, row, col):
        self.row = row
        self.col = col
        self.__order = "{}x{}".format(self.row,self.col)
        self.matrix = []
        for i in range(self.row):
            aux = []
            for i in range(self.col):
                aux.append(random.randint(0,9))
            self.matrix.append(aux)
    
    ##Print a Matrix
    def __str__(self):
        aux = "\n"
        for i in range(self.row):
            for j in range(self.col):
                aux += str(self.matrix[i][j])+" "
            aux += "\n"
        return aux

    def __getitm(self, i, j):
        return self.matrix[i][j]
    
    def __setitm(self, i, j, z):
        self.matrix[i][j] = z
    
    ##Sum
    def Suma(self, b):
        if (type(b) != type(self)):
            print ("No es una matriz")
            return None
        elif b.__order != self.__order:
            print ("No tenen el mateix ordre")
            return None
        
        c = Matrix(self.row, self.col)
        for i in range(self.row):
            for j in range(self.col):
                c.__setitm(i,j,(self.__getitm(i,j)+b.__getitm(i,j)))
        return c
    
    ##Resta
    def Resta(self, b):
        if (type(b) != type(self)):
            print ("No es una matriz")
            return None
        elif b.__order != self.__order:
            print ("No tenen el mateix ordre")
            return None
        
        c = Matrix(self.row, self.col)
        for i in range(self.row):
            for j in range(self.col):
                c.__setitm(i,j,(self.__getitm(i,j)-b.__getitm(i,j)))
        return c